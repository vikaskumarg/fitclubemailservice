﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitClubService
{
    public class DataManager
    {
        public static string ConnStr = "";

        SqlConnection g_DB;
        protected SqlCommand g_Cmd;
        SqlTransaction g_Tran;

        string gs_ConnStr;

        public DataManager()
        {
            if (DataManager.ConnStr == "")
            {
                DataManager.ConnStr = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStrings"].ToString();
            }
            gs_ConnStr = DataManager.ConnStr;
        }

        public enum ColumnType
        {
            CTBit = 1,
            CTDecimal = 2,
            CTInteger = 3,
            CTDate = 4,
            CTTime = 5,
            CTDateTime = 6,
            CTString = 7
        }

        public static DataTable DataTableCreate(string[] as_ColumnName, ColumnType[] a_ColumnType)
        {
            if (as_ColumnName.Length != a_ColumnType.Length)
            {
                return null;
            }

            DataTable l_dt = new DataTable();
            DataColumn l_col;

            for (int i = 0; i < a_ColumnType.Length; i++)
            {
                l_col = new DataColumn();
                switch (a_ColumnType[i])
                {
                    case ColumnType.CTBit:
                        l_col.DataType = System.Type.GetType("System.Boolean");
                        break;
                    case ColumnType.CTDecimal:
                        l_col.DataType = System.Type.GetType("System.Decimal");
                        break;
                    case ColumnType.CTInteger:
                        l_col.DataType = System.Type.GetType("System.Int32");
                        break;
                    case ColumnType.CTDate:
                    case ColumnType.CTDateTime:
                        l_col.DataType = System.Type.GetType("System.DateTime");
                        break;
                    default:
                        l_col.DataType = System.Type.GetType("System.String");
                        break;
                }
                l_col.ColumnName = as_ColumnName[i];
                l_dt.Columns.Add(l_col);
            }
            return l_dt;

        }

        public static string LocalCurrentTimestamp(decimal ad_GMT)
        {
            return "DATEADD(n, " + ad_GMT.ToString() + " * 60 ,GetUTCDate())";
        }


        public void ConnDB()
        {
            g_DB = new SqlConnection();
            g_DB.ConnectionString = gs_ConnStr;
            g_DB.Open();
            g_Cmd = new SqlCommand();
            g_Cmd.Connection = g_DB;
        }

        public void CloseDB()
        {
            g_DB.Close();
            g_Cmd = null;
            g_Tran = null;
        }

        public bool SQLHasRow(string as_SQL)
        {
            bool lb_Found = false;
            g_Cmd.CommandText = as_SQL;

            SqlDataReader da = g_Cmd.ExecuteReader();
            if (da.HasRows)
            {
                lb_Found = true;
            }
            da.Close();

            return lb_Found;
        }

        public void BeginTransaction()
        {
            g_Tran = g_DB.BeginTransaction();
            g_Cmd.Transaction = g_Tran;
        }

        public void Commit()
        {
            g_Tran.Commit();
        }

        public void RollBack()
        {
            g_Tran.Rollback();
        }

        public int Execute(string as_SQL)
        {
            g_Cmd.CommandText = as_SQL;
            return g_Cmd.ExecuteNonQuery();
        }

        public DataTable FetchDataTable(string as_SQL)
        {
            DataTable l_dt = new DataTable();
            g_Cmd.CommandText = as_SQL;
            SqlDataAdapter da = new SqlDataAdapter(g_Cmd);
            da.Fill(l_dt);
            return l_dt;
        }

        public string FetchString(string as_SQL)
        {
            g_Cmd.CommandText = as_SQL;
            object o = g_Cmd.ExecuteScalar();
            if (o == DBNull.Value || o == null)
            {
                return "";
            }
            else
            {
                return o.ToString();
            }
        }

        public int FetchInteger(string as_SQL)
        {
            g_Cmd.CommandText = as_SQL;
            object o = g_Cmd.ExecuteScalar();
            if (o == DBNull.Value || o == null)
            {
                return 0;
            }
            else
            {
                return int.Parse(o.ToString());
            }
        }

        public decimal FetchDecimal(string as_SQL)
        {
            g_Cmd.CommandText = as_SQL;
            object o = g_Cmd.ExecuteScalar();
            if (o == DBNull.Value || o == null)
            {
                return 0;
            }
            else
            {
                return decimal.Parse(o.ToString());
            }
        }


        public class StoreProBuilder
        {
            private DataManager g_DB;
            private string gs_ProcName;

            private List<string> g_OutputListName;
            private List<OutputType> g_OutputListType;
            private List<object> g_OutputListValue;

            private DataTable g_dt;

            public enum OutputType
            {
                OVarchar, OInteger, ODateTime, OMoney
            }

            public StoreProBuilder(string as_ProcName, DataManager a_DB)
            {
                g_DB = a_DB;
                gs_ProcName = as_ProcName;

                g_OutputListName = new List<string>();
                g_OutputListType = new List<OutputType>();
                g_OutputListValue = new List<object>();
                g_DB.g_Cmd.Parameters.Clear();
            }

            public void AddParameter(string as_ParamName, object a_Value)
            {
                g_DB.g_Cmd.Parameters.AddWithValue(as_ParamName, a_Value);
            }

            public void AddOutputParameter(string as_ParamName, OutputType a_Type)
            {
                g_OutputListName.Add(as_ParamName);
                g_OutputListType.Add(a_Type);
            }

            public void Execute()
            {
                g_DB.g_Cmd.CommandType = CommandType.StoredProcedure;
                g_DB.g_Cmd.CommandText = gs_ProcName;

                List<System.Data.SqlClient.SqlParameter> ls_PList = new List<System.Data.SqlClient.SqlParameter>();
                for (int i = 0; i < g_OutputListName.Count; i++)
                {
                    System.Data.SqlClient.SqlParameter p = null;

                    switch (g_OutputListType[i])
                    {
                        case OutputType.OVarchar:
                            p = g_DB.g_Cmd.Parameters.Add(g_OutputListName[i], SqlDbType.VarChar, 1000);
                            break;
                        case OutputType.OInteger:
                            p = g_DB.g_Cmd.Parameters.Add(g_OutputListName[i], SqlDbType.Int);
                            break;
                        case OutputType.OMoney:
                            p = g_DB.g_Cmd.Parameters.Add(g_OutputListName[i], SqlDbType.DateTime);
                            break;
                        case OutputType.ODateTime:
                            p = g_DB.g_Cmd.Parameters.Add(g_OutputListName[i], SqlDbType.Money);
                            break;
                    }
                    p.Direction = ParameterDirection.Output;
                    ls_PList.Add(p);
                }

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(g_DB.g_Cmd);
                g_dt = new DataTable();
                da.Fill(g_dt);

                g_DB.g_Cmd.CommandType = CommandType.Text;

                for (int i = 0; i < g_OutputListName.Count; i++)
                {
                    g_OutputListValue.Add(ls_PList[i].Value);
                }

                g_DB.g_Cmd.Parameters.Clear();
            }

            public object GetOutput(string as_Name)
            {
                for (int i = 0; i < g_OutputListName.Count; i++)
                {
                    if (g_OutputListName[i] == as_Name)
                    {
                        return g_OutputListValue[i];
                    }
                }

                return "";
            }

            public DataTable GetOutputDataTable()
            {
                return g_dt;
            }

        }

    }
}
