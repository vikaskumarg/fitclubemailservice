﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace FitClubService
{
    public partial class FitClubService : ServiceBase
    {
        private DateTime gd_LastRunTime;
        System.Timers.Timer t;
        string ls_ToListCoachName = "";
        public FitClubService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                WriteLog("Service started....");
                t = new System.Timers.Timer(1000);
                t.Elapsed += new System.Timers.ElapsedEventHandler(Process);
                t.Enabled = true;
                t.Start();
            }
            catch (Exception ex)
            {

                WriteLog(ex.Message);
            }
            
        }

        protected override void OnStop()
        {
            t.Enabled = false;
            WriteLog("Service stoped....");
        }

        private void Process(object sender, EventArgs e)
        {
            t.Enabled = false;
            try
            {
                bool lb_Run = false;
                if (gd_LastRunTime == null)
                {
                    lb_Run = true;
                }
                else
                {
                    if (DateTime.Now.Subtract(gd_LastRunTime).TotalMinutes > 1)
                    {
                        lb_Run = true;
                    }
                }

                if (lb_Run)
                {
                    gd_LastRunTime = DateTime.Now;
                    CoachRegistrationNotification();
                    CoachApprovalNotification();
                    CustomerRegistrationNotification();
                    ClassScheduleNotification();
                    BookingDetails();
                    BookingInformedToCustomer();
                    BookingDetailsCanceled();
                    ReminderToCustomer();
                    PassExpiryReminder();
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.Message);
            }
            finally
            {
                t.Enabled = true;
            }

        }
        public static void WriteLog(string as_Text)
        {
            string ls_File = ConfigurationManager.AppSettings["LogFile"].ToString();
            StreamWriter sw = new StreamWriter(ls_File, true);
            sw.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + as_Text);
            sw.Close();
        }

        private void PassExpiryReminder()
        {
            ls_ToListCoachName = "";
            string PassName = "";
            string CustEmail = "";
            string Name = "";
            int ExpiryReminder = 0;
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT * " +
                            "  FROM tbl_customerAndPassMapping a" +
                            " inner join Tbl_Customer b on a.Customer_id = b.id" +
                            " inner join LanguageMaster c on c.LanguageId = b.LanguageId" +
                            " WHERE a.IsEmailSent = 0 and a.Expirydate is not null";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        int CustomerId = Convert.ToInt16(l_dt.Rows[i]["Customer_id"].ToString());
                        int passId = Convert.ToInt16(l_dt.Rows[i]["Pass_id"].ToString());
                        string Language = l_dt.Rows[i]["LanguageName"].ToString();
                        DateTime Dt = new DateTime();
                        string Expirydate = "";
                        if (!string.IsNullOrEmpty(l_dt.Rows[i]["Expirydate"].ToString()))
                        {
                            Dt = Convert.ToDateTime(l_dt.Rows[i]["Expirydate"].ToString());
                            Expirydate = Dt.ToString("dd/MM/yyyy");

                        }
                        ls_SQL = "SELECT a.* " +
                            "  FROM tbl_PassMaster a" +
                            " WHERE a.Id  = " + passId +
                            "and a.IsActive = 1";

                        DataTable dt_pass = l_DB.FetchDataTable(ls_SQL);
                        if (dt_pass.Rows.Count > 0)
                        {
                            PassName = dt_pass.Rows[0]["Name"].ToString();
                            ExpiryReminder = Convert.ToInt16(dt_pass.Rows[0]["ExpiryReminder"].ToString());
                        }
                        int currentday = Convert.ToInt16((Dt - DateTime.Now).TotalDays);
                        if (currentday == ExpiryReminder)
                        {
                            ls_SQL = "SELECT a.* " +
                            "  FROM Tbl_Customer a" +
                            " WHERE a.Id  = " + CustomerId +
                            "and a.IsActive = 1";

                            DataTable dt_cust = l_DB.FetchDataTable(ls_SQL);
                            if (dt_cust.Rows.Count > 0)
                            {
                                CustEmail = dt_cust.Rows[0]["Email"].ToString();
                                Name = dt_cust.Rows[0]["Name"].ToString();
                            }
                            string Subject = ConfigurationManager.AppSettings["PassExpiry"].ToString();
                            string Body = PopulateBodyPassExpiry(Name, PassName, Expirydate, Language);
                            WriteLog("PassExpiryReminder....");
                            bool isSend = SmtpMail.SendEmail(CustEmail, Subject, Body);

                            if (isSend)
                            {
                                ls_SQL = "UPDATE tbl_customerAndPassMapping " +
                                         "   SET IsEmailSent     = 1" +
                                         "      ,EmailSentDate = GetUTCDate()" +
                                         " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                                l_DB.Execute(ls_SQL);
                            }
                        }

                    }
                    catch (Exception ex)
                    {

                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }
        private string PopulateBodyPassExpiry(string Name, string PassName, string Expirydate, string Language)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {
                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishPassExpiryReminder"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChinesePassExpiryReminder"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapanesePassExpiryReminder"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaPassExpiryReminder"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanPassExpiryReminder"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiPassExpiryReminder"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnamesePassExpiryReminder"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerPassExpiryReminder"].ToString();
                        break;
                }
                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{PassName}", PassName);
                body = body.Replace("{ExpiryDate}", Expirydate);
                body = body.Replace("{UserName}", Name);
            }
            catch (Exception ex)
            {
            }
            return body;

        }
        private void ReminderToCustomer()
        {
            ls_ToListCoachName = "";
            bool isSend = false;
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT * " +
                            "  FROM tbl_booking a" +
                            " inner join Tbl_Customer b on a.CustomerId = b.id" +
                            " inner join LanguageMaster c on c.LanguageId = b.LanguageId" +
                            " WHERE a.IsCustomerRemind = 0" +
                             "and a.IsCanceled = 1";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        string StartDate = "";
                        string Enddate = "";
                        string OneTime = "";
                        string Recurring = "";
                        //get class id from the booking table
                        int ClassId = Convert.ToInt16(l_dt.Rows[i]["ClassId"].ToString());
                        int CustomerId = Convert.ToInt16(l_dt.Rows[i]["CustomerId"].ToString());
                        int ClassScheduleId = Convert.ToInt16(l_dt.Rows[i]["ClassScheduleId"].ToString());
                        string Language = l_dt.Rows[i]["LanguageName"].ToString();
                        // get class start date
                        ls_SQL = "SELECT a.* " +
                            "  FROM tbl_ClassSchedule a" +
                            " WHERE a.IsActive = 1" +
                            " AND a.ClassId = " + ClassId;

                        DataTable l_dtclass = l_DB.FetchDataTable(ls_SQL);
                        if (l_dtclass.Rows.Count > 0)
                        {
                            DateTime Dt = DateTime.Today;
                            OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
                            Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
                            if (l_dtclass.Rows[0]["Type"].ToString() == Recurring)
                            {
                                Dt = Convert.ToDateTime(l_dtclass.Rows[0]["RecursiveStartDate"].ToString());
                            }
                            if (l_dtclass.Rows[0]["Type"].ToString() == OneTime)
                            {
                                Dt = Convert.ToDateTime(l_dtclass.Rows[0]["StartTime"].ToString());
                            }
                            int currentday = Convert.ToInt16((Dt - DateTime.UtcNow).TotalDays);
                            if (currentday == 1)
                            {
                                ls_SQL = "EXEC Sp_GetBookingDetails " + ClassId + "," + CustomerId + "," + ClassScheduleId + "";
                                DataTable dt_customer = l_DB.FetchDataTable(ls_SQL);
                                if (dt_customer.Rows.Count > 0)
                                {
                                    OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
                                    Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
                                    if (dt_customer.Rows[0]["Type"].ToString() == Recurring)
                                    {
                                        StartDate = (dt_customer.Rows[0]["RecursiveStartDate"].ToString());
                                        Enddate = (dt_customer.Rows[0]["RecursiveEndDate"].ToString());
                                    }
                                    if (dt_customer.Rows[0]["Type"].ToString() == OneTime)
                                    {
                                        StartDate = (dt_customer.Rows[0]["StartDate"].ToString());
                                        Enddate = (dt_customer.Rows[0]["Enddate"].ToString());
                                    }
                                    string ClassType = (dt_customer.Rows[0]["WordText"].ToString());
                                    string RecurringDay = (dt_customer.Rows[0]["RecurringDays"].ToString());
                                    string BookingDate = (dt_customer.Rows[0]["BookingDate"].ToString());
                                    string ClassName = dt_customer.Rows[0]["Classname"].ToString();
                                    string Location = dt_customer.Rows[0]["Address"].ToString();
                                    string CustomerName = (dt_customer.Rows[0]["CustomerName"].ToString());
                                    Boolean IsConfirmed = Convert.ToBoolean(dt_customer.Rows[0]["IsConfirmed"].ToString());
                                    string BookingStatus = "";
                                    if (IsConfirmed)
                                        BookingStatus = "Confirmed";
                                    else
                                        BookingStatus = "Waiting List";

                                    Boolean IsStatus = Convert.ToBoolean(dt_customer.Rows[0]["status"].ToString());
                                    string PassStatus = "";
                                    if (IsStatus)
                                        PassStatus = "Valid";
                                    else
                                        PassStatus = ConfigurationManager.AppSettings["PassStatus"].ToString();

                                    string Subject = ConfigurationManager.AppSettings["BookingSubjectReminder"].ToString();

                                    Boolean IsCanceled = Convert.ToBoolean(dt_customer.Rows[0]["IsCanceled"].ToString());

                                    string CustomerEmail = dt_customer.Rows[0]["CustomerEmail"].ToString();

                                    ls_SQL = "EXEC Sp_GetDataForCoachEmail " + ClassId + "," + ClassScheduleId + "";
                                    DataTable dt_coach = l_DB.FetchDataTable(ls_SQL);
                                    if (dt_coach.Rows.Count > 0)
                                    {
                                        string[] TobeDistinct = { "Name", "CoachEmail" };
                                        DataTable dtDistinct = GetDistinctRecords(dt_coach, TobeDistinct);
                                        foreach (DataRow l_dr in dtDistinct.Rows)
                                        {
                                            if (ls_ToListCoachName == "")
                                            {
                                                ls_ToListCoachName = l_dr["Name"].ToString();
                                            }
                                            else
                                            {
                                                ls_ToListCoachName += ", " + l_dr["Name"].ToString();
                                            }
                                        }
                                    }
                                    string CancelLink = "";
                                    ls_SQL = "EXEC Sp_DisplayCancelButton " + ClassId + "," + CustomerId + "," + ClassScheduleId + "";
                                    DataTable dt_cancel = l_DB.FetchDataTable(ls_SQL);
                                    if (dt_cancel.Rows.Count > 0)
                                        CancelLink = "";
                                    else
                                        CancelLink = GenerateLink(l_dt.Rows[i]["ClassId"].ToString(), l_dt.Rows[i]["CustomerId"].ToString(), l_dt.Rows[i]["ClassScheduleId"].ToString());
                                    string Body = PopulateBodyInformCustomer(ls_ToListCoachName, ClassName, Location, StartDate, Enddate, CustomerName, BookingStatus, PassStatus, IsCanceled, CancelLink, Language, ClassType, RecurringDay, BookingDate);
                                    WriteLog("ReminderToCustomer....");
                                    isSend = SmtpMail.SendEmail(CustomerEmail, Subject, Body);
                                    ls_ToListCoachName = "";
                                }
                                if (isSend)
                                {
                                    ls_SQL = "UPDATE tbl_booking " +
                                     "   SET IsCustomerRemind     = 1" +
                                     "      ,CistomerRemindDate = GetUTCDate()" +
                                     " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                                    l_DB.Execute(ls_SQL);
                                }

                            }

                        }
                    }
                    catch (Exception ex)
                    {

                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }

        private void BookingDetails()
        {
            ls_ToListCoachName = "";
            bool isSend = false;
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT a.* " +
                            "  FROM tbl_booking a" +
                            " WHERE a.IsEmailSent = 0" +
                             "and a.IsCanceled = 1";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        string StartDate = "";
                        string Enddate = "";
                        string OneTime = "";
                        string Recurring = "";
                        //get class id from the booking table
                        int ClassId = Convert.ToInt16(l_dt.Rows[i]["ClassId"].ToString());
                        int CustomerId = Convert.ToInt16(l_dt.Rows[i]["CustomerId"].ToString());
                        int ClassScheduleId = Convert.ToInt16(l_dt.Rows[i]["ClassScheduleId"].ToString());
                        ls_SQL = "EXEC Sp_GetDataForCoachEmail " + ClassId + "," + ClassScheduleId + "";
                        DataTable dt_coach = l_DB.FetchDataTable(ls_SQL);
                        if (dt_coach.Rows.Count > 0)
                        {
                            ls_SQL = "EXEC Sp_GetBookingDetails " + ClassId + "," + CustomerId + "," + ClassScheduleId + "";
                            DataTable dt_customer = l_DB.FetchDataTable(ls_SQL);
                            if (dt_customer.Rows.Count > 0)
                            {
                                OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
                                Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
                                if (dt_customer.Rows[0]["Type"].ToString() == Recurring)
                                {
                                    StartDate = (dt_customer.Rows[0]["RecursiveStartDate"].ToString());
                                    Enddate = (dt_customer.Rows[0]["RecursiveEndDate"].ToString());
                                }
                                if (dt_customer.Rows[0]["Type"].ToString() == OneTime)
                                {
                                    StartDate = (dt_customer.Rows[0]["StartDate"].ToString());
                                    Enddate = (dt_customer.Rows[0]["Enddate"].ToString());
                                }
                                string ClassType = (dt_customer.Rows[0]["WordText"].ToString());
                                string RecurringDay = (dt_customer.Rows[0]["RecurringDays"].ToString());
                                string BookingDate = (dt_customer.Rows[0]["BookingDate"].ToString());
                                string ClassName = dt_customer.Rows[0]["Classname"].ToString();
                                string Location = dt_customer.Rows[0]["Address"].ToString();
                                string CustomerName = (dt_customer.Rows[0]["CustomerName"].ToString());
                                Boolean IsConfirmed = Convert.ToBoolean(dt_customer.Rows[0]["IsConfirmed"].ToString());
                                string BookingStatus = "";
                                if (IsConfirmed)
                                    BookingStatus = "Confirmed";
                                else
                                    BookingStatus = "Waiting List";

                                Boolean IsStatus = Convert.ToBoolean(dt_customer.Rows[0]["status"].ToString());
                                string PassStatus = "";
                                if (IsStatus)
                                    PassStatus = "Valid";
                                else
                                    PassStatus = ConfigurationManager.AppSettings["PassStatus"].ToString();

                                string Subject = ConfigurationManager.AppSettings["BookingSubject"].ToString();

                                Boolean IsCanceled = Convert.ToBoolean(dt_customer.Rows[0]["IsCanceled"].ToString());
                                for (int j = 0; j < dt_coach.Rows.Count; j++)
                                {
                                    string CoachName = dt_coach.Rows[j]["Name"].ToString();
                                    string CoachEmail = dt_coach.Rows[j]["CoachEmail"].ToString();
                                    string Language = dt_coach.Rows[j]["LanguageName"].ToString();
                                    WriteLog("BookingDetails....");
                                    if (!string.IsNullOrEmpty(CoachEmail))
                                    {
                                        string Body = PopulateBodyBookingCoach(CoachName, ClassName, Location, StartDate, Enddate, CustomerName, BookingStatus, PassStatus, IsCanceled, Language, ClassType, RecurringDay, BookingDate);
                                        isSend = SmtpMail.SendEmail(CoachEmail, Subject, Body);
                                    }
                                       
                                }
                            }
                        }
                        if (isSend)
                        {
                            ls_SQL = "UPDATE tbl_booking " +
                                     "   SET IsEmailSent     = 1" +
                                     "      ,EmailSentDate = GetUTCDate()" +
                                     " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                            l_DB.Execute(ls_SQL);
                        }
                    }
                    catch (Exception ex)
                    {

                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }
        private void BookingInformedToCustomer()
        {
            ls_ToListCoachName = "";
            bool isSend = false;
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT * " +
                            "  FROM tbl_booking a" +
                            " inner join Tbl_Customer b on a.CustomerId = b.id" +
                            " inner join LanguageMaster c on c.LanguageId = b.LanguageId" +
                            " WHERE a.IsInformedCustomer = 0" +
                             "and a.IsCanceled = 1";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        string StartDate = "";
                        string Enddate = "";
                        //get class id from the booking table
                        int ClassId = Convert.ToInt16(l_dt.Rows[i]["ClassId"].ToString());
                        int CustomerId = Convert.ToInt16(l_dt.Rows[i]["CustomerId"].ToString());
                        int ClassScheduleId = Convert.ToInt16(l_dt.Rows[i]["ClassScheduleId"].ToString());
                        string Language = l_dt.Rows[i]["LanguageName"].ToString();
                        ls_SQL = "EXEC Sp_GetBookingDetails " + ClassId + "," + CustomerId + "," + ClassScheduleId + "";

                        DataTable dt_customer = l_DB.FetchDataTable(ls_SQL);
                        if (dt_customer.Rows.Count > 0)
                        {
                            string OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
                            string Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
                            if (dt_customer.Rows[0]["Type"].ToString() == Recurring)
                            {
                                StartDate = (dt_customer.Rows[0]["RecursiveStartDate"].ToString());
                                Enddate = (dt_customer.Rows[0]["RecursiveEndDate"].ToString());
                            }
                            if (dt_customer.Rows[0]["Type"].ToString() == OneTime)
                            {
                                StartDate = (dt_customer.Rows[0]["StartDate"].ToString());
                                Enddate = (dt_customer.Rows[0]["Enddate"].ToString());
                            }
                            string ClassName = dt_customer.Rows[0]["Classname"].ToString();
                            string Location = dt_customer.Rows[0]["Address"].ToString();
                            string ClassType = (dt_customer.Rows[0]["WordText"].ToString());
                            string RecurringDay = (dt_customer.Rows[0]["RecurringDays"].ToString());
                            string BookingDate = (dt_customer.Rows[0]["BookingDate"].ToString());
                            string CustomerName = (dt_customer.Rows[0]["CustomerName"].ToString());
                            Boolean IsConfirmed = Convert.ToBoolean(dt_customer.Rows[0]["IsConfirmed"].ToString());
                            string BookingStatus = "";
                            if (IsConfirmed)
                                BookingStatus = "Confirmed";
                            else
                                BookingStatus = "Waiting List";

                            Boolean IsStatus = Convert.ToBoolean(dt_customer.Rows[0]["status"].ToString());
                            string PassStatus = "";
                            if (IsStatus)
                                PassStatus = "Valid";
                            else
                                PassStatus = ConfigurationManager.AppSettings["PassStatus"].ToString();

                            string Subject = ConfigurationManager.AppSettings["BookingSubject"].ToString();

                            Boolean IsCanceled = Convert.ToBoolean(dt_customer.Rows[0]["IsCanceled"].ToString());

                            string CustomerEmail = dt_customer.Rows[0]["CustomerEmail"].ToString();

                            ls_SQL = "EXEC Sp_GetDataForCoachEmail " + ClassId + "," + ClassScheduleId + "";
                            DataTable dt_coach = l_DB.FetchDataTable(ls_SQL);
                            if (dt_coach.Rows.Count > 0)
                            {
                                string[] TobeDistinct = { "Name", "CoachEmail" };
                                DataTable dtDistinct = GetDistinctRecords(dt_coach, TobeDistinct);
                                foreach (DataRow l_dr in dtDistinct.Rows)
                                {
                                    if (ls_ToListCoachName == "")
                                    {
                                        ls_ToListCoachName = l_dr["Name"].ToString();
                                    }
                                    else
                                    {
                                        ls_ToListCoachName += ", " + l_dr["Name"].ToString();
                                    }
                                }
                            }
                            string CancelLink = "";
                            ls_SQL = "EXEC Sp_DisplayCancelButton " + ClassId + "," + CustomerId + "," + ClassScheduleId + "";
                            DataTable dt_cancel = l_DB.FetchDataTable(ls_SQL);
                            if (dt_cancel.Rows.Count > 0)
                                CancelLink = "";
                            else
                                CancelLink = GenerateLink(l_dt.Rows[i]["ClassId"].ToString(), l_dt.Rows[i]["CustomerId"].ToString(), l_dt.Rows[i]["ClassScheduleId"].ToString());
                            string Body = PopulateBodyInformCustomer(ls_ToListCoachName, ClassName, Location, StartDate, Enddate, CustomerName, BookingStatus, PassStatus, IsCanceled, CancelLink, Language, ClassType, RecurringDay, BookingDate);
                            WriteLog("BookingInformedToCustomer....");
                            isSend =  SmtpMail.SendEmail(CustomerEmail, Subject, Body);
                            ls_ToListCoachName = "";
                        }
                        if (isSend)
                        {
                            ls_SQL = "UPDATE tbl_booking " +
                                     "   SET IsInformedCustomer     = 1" +
                                     "      ,InformedDate = GetUTCDate()" +
                                     " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                            l_DB.Execute(ls_SQL);
                        }
                    }
                    catch (Exception ex)
                    {

                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }
        private void BookingDetailsCanceled()
        {
            ls_ToListCoachName = "";
            bool isSend = false;
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT * " +
                            "  FROM tbl_booking a" +
                            " inner join Tbl_Customer b on a.CustomerId = b.id" +
                            " inner join LanguageMaster c on c.LanguageId = b.LanguageId" +
                            " WHERE a.IsBookingCanceled = 0" +
                             "and a.IsCanceled = 0";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        string StartDate = "";
                        string Enddate = "";
                        //get class id from the booking table
                        int ClassId = Convert.ToInt16(l_dt.Rows[i]["ClassId"].ToString());
                        int CustomerId = Convert.ToInt16(l_dt.Rows[i]["CustomerId"].ToString());
                        int ClassScheduleId = Convert.ToInt16(l_dt.Rows[i]["ClassScheduleId"].ToString());
                        string Language = l_dt.Rows[i]["LanguageName"].ToString();
                        ls_SQL = "EXEC Sp_GetBookingDetails " + ClassId + "," + CustomerId + "," + ClassScheduleId + "";
                        DataTable dt_customer = l_DB.FetchDataTable(ls_SQL);
                        if (dt_customer.Rows.Count > 0)
                        {
                            string OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
                            string Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
                            if (dt_customer.Rows[0]["Type"].ToString() == Recurring)
                            {
                                StartDate = (dt_customer.Rows[0]["RecursiveStartDate"].ToString());
                                Enddate = (dt_customer.Rows[0]["RecursiveEndDate"].ToString());
                            }
                            if (dt_customer.Rows[0]["Type"].ToString() == OneTime)
                            {
                                StartDate = (dt_customer.Rows[0]["StartDate"].ToString());
                                Enddate = (dt_customer.Rows[0]["Enddate"].ToString());
                            }
                            string ClassName = dt_customer.Rows[0]["Classname"].ToString();
                            string Location = dt_customer.Rows[0]["Address"].ToString();
                            string ClassType = (dt_customer.Rows[0]["WordText"].ToString());
                            string RecurringDay = (dt_customer.Rows[0]["RecurringDays"].ToString());
                            string BookingDate = (dt_customer.Rows[0]["BookingDate"].ToString());
                            string CustomerName = (dt_customer.Rows[0]["CustomerName"].ToString());
                            Boolean IsConfirmed = Convert.ToBoolean(dt_customer.Rows[0]["IsConfirmed"].ToString());
                            string BookingStatus = "";
                            if (IsConfirmed)
                                BookingStatus = "Confirmed";
                            else
                                BookingStatus = "Waiting List";

                            Boolean IsStatus = Convert.ToBoolean(dt_customer.Rows[0]["status"].ToString());
                            string PassStatus = "";
                            if (IsStatus)
                                PassStatus = "Valid";
                            else
                                PassStatus = ConfigurationManager.AppSettings["PassStatus"].ToString();

                            string Subject = ConfigurationManager.AppSettings["BookingCanceld"].ToString();

                            Boolean IsCanceled = Convert.ToBoolean(dt_customer.Rows[0]["IsCanceled"].ToString());

                            string CustomerEmail = dt_customer.Rows[0]["CustomerEmail"].ToString();

                            ls_SQL = "EXEC Sp_GetDataForCoachEmail " + ClassId + "," + ClassScheduleId + "";
                            DataTable dt_coach = l_DB.FetchDataTable(ls_SQL);
                            if (dt_coach.Rows.Count > 0)
                            {
                                string[] TobeDistinct = { "Name", "CoachEmail" };
                                DataTable dtDistinct = GetDistinctRecords(dt_coach, TobeDistinct);
                                foreach (DataRow l_dr in dtDistinct.Rows)
                                {
                                    if (ls_ToListCoachName == "")
                                    {
                                        ls_ToListCoachName = l_dr["Name"].ToString();
                                    }
                                    else
                                    {
                                        ls_ToListCoachName += ", " + l_dr["Name"].ToString();
                                    }
                                }
                            }
                            WriteLog("BookingDetailsCanceled....");
                            string Body = PopulateBodyCancelBooking(ls_ToListCoachName, ClassName, Location, StartDate, Enddate, CustomerName, BookingStatus, PassStatus, IsCanceled, Language, ClassType, RecurringDay, BookingDate);
                            isSend =  SmtpMail.SendEmail(CustomerEmail, Subject, Body);
                            ls_ToListCoachName = "";
                        }
                        if (isSend)
                        {
                            ls_SQL = "UPDATE tbl_booking " +
                                     "   SET IsBookingCanceled     = 1" +
                                     "      ,CanceledDate = GetUTCDate()" +
                                     " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                            l_DB.Execute(ls_SQL);
                        }
                    }
                    catch (Exception ex)
                    {

                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }
        private string PopulateBodyBookingCoach(string CoachName, string ClassName, string Location, string StartDate, string Enddate, string CustomerName, string BookingStatus, string PassStatus, Boolean IsCanceled, string Language, string ClassType, string RecurringDay, string BookingDate)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {

                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishBookingDetailsTemp"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChineseBookingDetailsTemp"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapaneseBookingDetailsTemp"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaBookingDetailsTemp"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanBookingDetailsTemp"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiBookingDetailsTemp"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnameseBookingDetailsTemp"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerBookingDetailsTemp"].ToString();
                        break;
                }

                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{ClassName}", ClassName);
                body = body.Replace("{ClassType}", ClassType);
                body = body.Replace("{RecurringDays}", RecurringDay);
                body = body.Replace("{BookingDate}", BookingDate);
                body = body.Replace("{Location}", Location);
                body = body.Replace("{StartDate}", StartDate);
                body = body.Replace("{EndDate}", Enddate);
                body = body.Replace("{CustomerName}", CustomerName);
                body = body.Replace("{BookingStatus}", BookingStatus);
                body = body.Replace("{PassStatus}", PassStatus);
                body = body.Replace("{UserName}", CoachName);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return body;

        }
        private string PopulateBodyCancelBooking(string CoachName, string ClassName, string Location, string StartDate, string Enddate, string CustomerName, string BookingStatus, string PassStatus, Boolean IsCanceled, string Language, string ClassType, string RecurringDay, string BookingDate)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {
                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishBookingCancelDetailsTemp"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChineseBookingCancelDetailsTemp"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapaneseBookingCancelDetailsTemp"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaBookingCancelDetailsTemp"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanBookingCancelDetailsTemp"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiBookingCancelDetailsTemp"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnameseBookingCancelDetailsTemp"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerBookingCancelDetailsTemp"].ToString();
                        break;
                }

                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{ClassName}", ClassName);
                body = body.Replace("{ClassType}", ClassType);
                body = body.Replace("{RecurringDays}", RecurringDay);
                body = body.Replace("{BookingDate}", BookingDate);
                body = body.Replace("{Location}", Location);
                body = body.Replace("{StartDate}", StartDate);
                body = body.Replace("{EndDate}", Enddate);
                body = body.Replace("{CoachName}", CoachName);
                body = body.Replace("{BookingStatus}", BookingStatus);
                body = body.Replace("{PassStatus}", PassStatus);
                body = body.Replace("{UserName}", CustomerName);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return body;

        }
        public string GenerateLink(string ClassId, string CustomerId,string ClassScheduleId)
        {
            string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["BaseUrl"].ToString();
            string mylink = BaseUrl + "/" + ClassId + "/" + CustomerId + "/" + ClassScheduleId;
            return "<a href= " + mylink + " >Cancel Booking</a>";

        }
        private string PopulateBodyInformCustomer(string CoachName, string ClassName, string Location, string StartDate, string Enddate, string CustomerName, string BookingStatus, string PassStatus, Boolean IsCanceled, string CancelBooking, string Language, string ClassType, string RecurringDay, string BookingDate)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {
                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishCustomerBooking"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChineseCustomerBooking"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapaneseCustomerBooking"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaCustomerBooking"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanCustomerBooking"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiCustomerBooking"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnameseCustomerBooking"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerCustomerBooking"].ToString();
                        break;
                }

                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{ClassName}", ClassName);
                body = body.Replace("{ClassType}", ClassType);
                body = body.Replace("{RecurringDays}", RecurringDay);
                body = body.Replace("{BookingDate}", BookingDate);
                body = body.Replace("{Location}", Location);
                body = body.Replace("{StartDate}", StartDate);
                body = body.Replace("{EndDate}", Enddate);
                body = body.Replace("{CoachName}", CoachName);
                body = body.Replace("{BookingStatus}", BookingStatus);
                body = body.Replace("{PassStatus}", PassStatus);
                body = body.Replace("{UserName}", CustomerName);
                body = body.Replace("@CancelBooking", CancelBooking);

            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return body;

        }

        private void CoachRegistrationNotification()
        {
            ls_ToListCoachName = "";
            bool isSend = false;
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT a.*, u.UserId,ue.CountryId,l.LanguageName " +
                            "  FROM tbl_Coaches a" +
                            " inner join tblUser u on u.MemberId = a.MemberId" +
                            " inner join tblUserExtended ue on ue.UserId = u.UserId" +
                            " inner join CountryLanguageMappingMaster cl on cl.CountryId = ue.CountryId" +
                            " inner join LanguageMaster l on l.LanguageId = cl.LanguageId" +
                            " WHERE a.IsEmailSent = 0" +
                            " and a.IsApproved is null and IsOriginalOwner is null and u.IsNormalUser = 1";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        int Id = Convert.ToInt16(l_dt.Rows[i]["Id"].ToString());
                        string Name = l_dt.Rows[i]["Name"].ToString();
                        string Email = l_dt.Rows[i]["Email"].ToString();
                        string Language = l_dt.Rows[i]["LanguageName"].ToString();
                        string Status = "";
                        if (string.IsNullOrEmpty(l_dt.Rows[i]["IsApproved"].ToString()))
                            Status = "waiting for approval";

                        string CoachRegistration = ConfigurationManager.AppSettings["CoachRegistration"].ToString();
                        string Body = PopulateBodyCoachReg(Name, Status, Language);
                        WriteLog("CoachRegistrationNotification....");
                        isSend = SmtpMail.SendEmail(Email, CoachRegistration, Body);
                        if (isSend)
                        {
                            ls_SQL = "UPDATE tbl_Coaches " +
                                     "   SET IsEmailSent     = 1" +
                                     "      ,EmailSentDate = GetUTCDate()" +
                                     " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                            l_DB.Execute(ls_SQL);
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }
        private void CoachApprovalNotification()
        {
            ls_ToListCoachName = "";
            bool isSend = false;
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT a.*, u.UserId,ue.CountryId,l.LanguageName " +
                            "  FROM tbl_Coaches a" +
                            " inner join tblUser u on u.MemberId = a.MemberId" +
                            " inner join tblUserExtended ue on ue.UserId = u.UserId" +
                            " inner join CountryLanguageMappingMaster cl on cl.CountryId = ue.CountryId" +
                            " inner join LanguageMaster l on l.LanguageId = cl.LanguageId" +
                            " WHERE a.IsApprovalSent = 0" +
                            " and a.IsApproved is not null and IsOriginalOwner is null and u.IsNormalUser = 1";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        int Id = Convert.ToInt16(l_dt.Rows[i]["Id"].ToString());
                        string Name = l_dt.Rows[i]["Name"].ToString();
                        string Email = l_dt.Rows[i]["Email"].ToString();
                        string Language = l_dt.Rows[i]["LanguageName"].ToString();
                        string Status = "";
                        if (Convert.ToBoolean(l_dt.Rows[i]["IsApproved"].ToString()) == true)
                            Status = "approved";
                        if (Convert.ToBoolean(l_dt.Rows[i]["IsApproved"].ToString()) == false)
                            Status = "rejected";
                        string CoachRegistration = ConfigurationManager.AppSettings["Status"].ToString();
                        string Body = PopulateBodyCoachReg(Name, Status, Language);
                        WriteLog("CoachApprovalNotification....");
                        isSend = SmtpMail.SendEmail(Email, CoachRegistration, Body);
                        if (isSend)
                        {
                            ls_SQL = "UPDATE tbl_Coaches " +
                                     "   SET IsApprovalSent     = 1" +
                                     "      ,ApprovalSentDate = GetUTCDate()" +
                                     " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                            l_DB.Execute(ls_SQL);
                        }
                    }
                    catch (Exception ex)
                    {

                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }
        private string PopulateBodyCoachReg(string Name, string Status, string Language)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {
                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishCoachRegistrationTemp"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChineseCoachRegistrationTemp"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapaneseCoachRegistrationTemp"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaCoachRegistrationTemp"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanCoachRegistrationTemp"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiCoachRegistrationTemp"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnameseCoachRegistrationTemp"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerCoachRegistrationTemp"].ToString();
                        break;
                }
                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{Status}", Status);
                body = body.Replace("{UserName}", Name);
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
            return body;

        }

        private void CustomerRegistrationNotification()
        {
            ls_ToListCoachName = "";
            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT * " +
                            "  FROM Tbl_Customer a" +
                            " inner join LanguageMaster c on c.LanguageId = a.LanguageId" +
                            " WHERE a.IsEmailSent = 0";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        int Id = Convert.ToInt16(l_dt.Rows[i]["Id"].ToString());
                        string Language = l_dt.Rows[i]["LanguageName"].ToString();
                        string Name = l_dt.Rows[i]["Name"].ToString();
                        string Email = l_dt.Rows[i]["Email"].ToString();
                        string Subject = ConfigurationManager.AppSettings["CustomerRegistration"].ToString();
                        string Body = PopulateBodyCustomerReg(Name, Language);
                        WriteLog("CustomerRegistrationNotification....");
                        bool isSend = SmtpMail.SendEmail(Email, Subject, Body);
                        if (isSend)
                        {
                            ls_SQL = "UPDATE Tbl_Customer " +
                                     "   SET IsEmailSent     = 1" +
                                     "      ,EmailSentDate = GetUTCDate()" +
                                     " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                            l_DB.Execute(ls_SQL);
                        }
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                }
            }

            l_DB.CloseDB();
        }
        private string PopulateBodyCustomerReg(string Name, string Language)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {
                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishCustomerRegistrationTemp"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChineseCustomerRegistrationTemp"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapaneseCustomerRegistrationTemp"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaCustomerRegistrationTemp"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanCustomerRegistrationTemp"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiCustomerRegistrationTemp"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnameseCustomerRegistrationTemp"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerCustomerRegistrationTemp"].ToString();
                        break;
                }
                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", Name);
            }
            catch (Exception ex)
            {
                WriteLog("Populate body");
            }
            return body;

        }

        private void ClassScheduleNotification()
        {
            ls_ToListCoachName = "";

            DataManager l_DB = new DataManager();
            l_DB.ConnDB();

            string ls_SQL = "SELECT a.* " +
                            "  FROM tbl_ClassSchedule a" +
                            " WHERE a.IsEmailSent = 0 or a.IsEmailSent is null";

            DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

            if (l_dt.Rows.Count > 0)
            {
                for (int i = 0; i < l_dt.Rows.Count; i++)
                {
                    try
                    {
                        bool isSend = false;
                        bool isActive = false;
                        string ClassDeleted = string.Empty;
                        string Body = "";
                        string ClassName = "";
                        string Location = "";
                        string StartDate = "";
                        string Enddate = "";
                        string Remarks = "";
                        string ClassType = "";
                        string RecurringDay = "";
                        string BookingDate = "";
                        int ClassId = Convert.ToInt16(l_dt.Rows[i]["ClassId"].ToString());
                        int ClassScheduleId = Convert.ToInt16(l_dt.Rows[i]["Id"].ToString());
                        //get customer from the class
                        ls_SQL = "EXEC Sp_GetDataForClassEmail " + ClassId + "," + ClassScheduleId + "";
                        DataTable dt_coach = l_DB.FetchDataTable(ls_SQL);
                        //get coach of class
                        ls_SQL = "EXEC Sp_GetDataForCoachEmail " + ClassId + "," + ClassScheduleId + "";
                        DataTable dt_class = l_DB.FetchDataTable(ls_SQL);
                        if (dt_class.Rows.Count > 0)
                        {
                            for (int j = 0; j < dt_class.Rows.Count; j++)
                            {
                                string OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
                                string Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
                                if (dt_class.Rows[j]["Type"].ToString() == Recurring)
                                {
                                    StartDate = (dt_class.Rows[j]["RecursiveStartDate"].ToString());
                                    Enddate = (dt_class.Rows[j]["RecursiveEndDate"].ToString());
                                }
                                if (dt_class.Rows[j]["Type"].ToString() == OneTime)
                                {
                                    StartDate = (dt_class.Rows[j]["StartDate"].ToString());
                                    Enddate = (dt_class.Rows[j]["Enddate"].ToString());
                                }
                                ClassName = (dt_class.Rows[j]["Classname"].ToString());
                                Location = (dt_class.Rows[j]["Address"].ToString());
                                //StartDate = (dt_class.Rows[j]["StartDate"].ToString());
                                //Enddate = (dt_class.Rows[j]["Enddate"].ToString());
                                
                                Remarks = (dt_class.Rows[j]["Remark"].ToString());
                                string Language = (dt_class.Rows[j]["LanguageName"].ToString());
                                ClassType = (dt_class.Rows[j]["WordText"].ToString());
                                RecurringDay = (dt_class.Rows[j]["RecurringDays"].ToString());
                                //BookingDate = (dt_class.Rows[j]["BookingDate"].ToString());

                                isActive = Convert.ToBoolean(dt_class.Rows[j]["IsActive"].ToString());
                                if (isActive)
                                    ClassDeleted = ConfigurationManager.AppSettings["ClassActivated"].ToString();
                                else
                                    ClassDeleted = ConfigurationManager.AppSettings["ClassDeactivated"].ToString();
                                //Body = PopulateBodyClassForCustomer(ClassName, Coach, Location, StartDate, Enddate, CustomerName, Remarks, Language, ClassDeleted, ClassType, RecurringDay, BookingDate);

                                //isSend = SmtpMail.SendEmail(CustomerEmail, ClassDeleted, Body);
                                string CoachName = (dt_class.Rows[j]["Name"].ToString());
                                string CoachEmail = (dt_class.Rows[j]["CoachEmail"].ToString());
                                Language = (dt_class.Rows[j]["LanguageName"].ToString());

                                Body = PopulateBodyClassForCoach(ClassName, Location, StartDate, Enddate, CoachName, Remarks, Language, ClassDeleted, ClassType, RecurringDay, BookingDate);
                                WriteLog("ClassScheduleNotification....");
                                if (!string.IsNullOrEmpty(CoachEmail))
                                {
                                    isSend = SmtpMail.SendEmail(CoachEmail, ClassDeleted, Body);
                                }
                                    
                            }

                        }
                        if (dt_coach.Rows.Count > 0)
                        {
                            for (int k = 0; k < dt_coach.Rows.Count; k++)
                            {
                                string OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
                                string Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
                                if (dt_coach.Rows[k]["Type"].ToString() == Recurring)
                                {
                                    StartDate = (dt_coach.Rows[k]["RecursiveStartDate"].ToString());
                                    Enddate = (dt_coach.Rows[k]["RecursiveEndDate"].ToString());
                                }
                                if (dt_coach.Rows[k]["Type"].ToString() == OneTime)
                                {
                                    StartDate = (dt_coach.Rows[k]["StartDate"].ToString());
                                    Enddate = (dt_coach.Rows[k]["Enddate"].ToString());
                                }
                                if (dt_coach.Rows.Count > 0)
                                {
                                    string[] TobeDistinct = { "Name", "CoachEmail" };
                                    DataTable dtDistinct = GetDistinctRecords(dt_class, TobeDistinct);
                                    foreach (DataRow l_dr in dtDistinct.Rows)
                                    {
                                        if (ls_ToListCoachName == "")
                                        {
                                            ls_ToListCoachName = l_dr["Name"].ToString();
                                        }
                                        else
                                        {
                                            ls_ToListCoachName += ", " + l_dr["Name"].ToString();
                                        }
                                    }
                                }

                                ClassName = (dt_coach.Rows[k]["Classname"].ToString());
                                Location = (dt_coach.Rows[k]["Address"].ToString());
                                Remarks = (dt_coach.Rows[k]["Remark"].ToString());
                                string Language = (dt_coach.Rows[k]["LanguageName"].ToString());
                                ClassType = (dt_coach.Rows[k]["WordText"].ToString());
                                RecurringDay = (dt_coach.Rows[k]["RecurringDays"].ToString());
                                BookingDate = (dt_coach.Rows[k]["BookingDate"].ToString());
                                string CustomerName = (dt_coach.Rows[k]["CustomerName"].ToString());
                                string CustomerEmail = (dt_coach.Rows[k]["CustomerEmail"].ToString());
                                string Coach = ls_ToListCoachName;

                                isActive = Convert.ToBoolean(dt_coach.Rows[k]["IsActive"].ToString());
                                if (isActive)
                                    ClassDeleted = ConfigurationManager.AppSettings["ClassActivated"].ToString();
                                else
                                    ClassDeleted = ConfigurationManager.AppSettings["ClassDeactivated"].ToString();

                                Body = PopulateBodyClassForCustomer(ClassName, Coach, Location, StartDate, Enddate, CustomerName, Remarks, Language, ClassDeleted, ClassType, RecurringDay, BookingDate);

                                SmtpMail.SendEmail(CustomerEmail, ClassDeleted, Body);
                                ls_ToListCoachName = "";

                            }
                        }

                        ls_SQL = "UPDATE tbl_ClassSchedule " +
                                 "   SET IsEmailSent     = 1" +
                                 "      ,EmailSentDate = GetUTCDate()" +
                                 " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
                        l_DB.Execute(ls_SQL);
                    }
                    catch (Exception ex)
                    {
                        WriteLog(ex.ToString());
                    }
                }
            }

            l_DB.CloseDB();
        }
        //private void ClassScheduleNotification()
        //{
        //    ls_ToListCoachName = "";

        //    DataManager l_DB = new DataManager();
        //    l_DB.ConnDB();

        //    string ls_SQL = "SELECT a.* " +
        //                    "  FROM tbl_ClassSchedule a" +
        //                    " WHERE a.IsEmailSent = 0 or a.IsEmailSent is null";

        //    DataTable l_dt = l_DB.FetchDataTable(ls_SQL);

        //    if (l_dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < l_dt.Rows.Count; i++)
        //        {
        //            try
        //            {
        //                bool isSend = false;
        //                bool isActive = false;
        //                string ClassDeleted = string.Empty;
        //                string Body = "";
        //                string ClassName = "";
        //                string Location = "";
        //                string StartDate = "";
        //                string Enddate = "";
        //                string Remarks = "";
        //                string ClassType = "";
        //                string RecurringDay = "";
        //                string BookingDate = "";
        //                int ClassId = Convert.ToInt16(l_dt.Rows[i]["ClassId"].ToString());
        //                int ClassScheduleId = Convert.ToInt16(l_dt.Rows[i]["Id"].ToString());
        //                //get customer from the class
        //                ls_SQL = "EXEC Sp_GetDataForClassEmail " + ClassId + "," + ClassScheduleId + "";
        //                DataTable dt_class = l_DB.FetchDataTable(ls_SQL);
        //                //get coach of class
        //                ls_SQL = "EXEC Sp_GetDataForCoachEmail " + ClassId + "," + ClassScheduleId + "";
        //                DataTable dt_coch = l_DB.FetchDataTable(ls_SQL);
        //                if (dt_class.Rows.Count > 0)
        //                {
        //                    if (dt_coch.Rows.Count > 0)
        //                    {
        //                        string[] TobeDistinct = { "Name", "CoachEmail" };
        //                        DataTable dtDistinct = GetDistinctRecords(dt_coch, TobeDistinct);
        //                        foreach (DataRow l_dr in dtDistinct.Rows)
        //                        {
        //                            if (ls_ToListCoachName == "")
        //                            {
        //                                ls_ToListCoachName = l_dr["Name"].ToString();
        //                            }
        //                            else
        //                            {
        //                                ls_ToListCoachName += ", " + l_dr["Name"].ToString();
        //                            }
        //                        }
        //                    }
        //                    for (int j = 0; j < dt_class.Rows.Count; j++)
        //                    {
        //                        string OneTime = ConfigurationManager.AppSettings["OneTime"].ToString();
        //                        string Recurring = ConfigurationManager.AppSettings["Recurring"].ToString();
        //                        if (dt_class.Rows[j]["Type"].ToString() == Recurring)
        //                        {
        //                            StartDate = (dt_class.Rows[j]["RecursiveStartDate"].ToString());
        //                            Enddate = (dt_class.Rows[j]["RecursiveEndDate"].ToString());
        //                        }
        //                        if (dt_class.Rows[j]["Type"].ToString() == OneTime)
        //                        {
        //                            StartDate = (dt_class.Rows[j]["StartDate"].ToString());
        //                            Enddate = (dt_class.Rows[j]["Enddate"].ToString());
        //                        }
        //                        ClassName = (dt_class.Rows[j]["Classname"].ToString());
        //                        string Coach = ls_ToListCoachName;
        //                        Location = (dt_class.Rows[j]["Address"].ToString());
        //                        //StartDate = (dt_class.Rows[j]["StartDate"].ToString());
        //                        //Enddate = (dt_class.Rows[j]["Enddate"].ToString());
        //                        string CustomerName = (dt_class.Rows[j]["CustomerName"].ToString());
        //                        Remarks = (dt_class.Rows[j]["Remark"].ToString());
        //                        string CustomerEmail = (dt_class.Rows[j]["CustomerEmail"].ToString());
        //                        string Language = (dt_class.Rows[j]["LanguageName"].ToString());
        //                        ClassType = (dt_class.Rows[j]["WordText"].ToString());
        //                        RecurringDay = (dt_class.Rows[j]["RecurringDays"].ToString());
        //                        BookingDate = (dt_class.Rows[j]["BookingDate"].ToString());

        //                        isActive = Convert.ToBoolean(dt_class.Rows[j]["IsActive"].ToString());
        //                        if (isActive)
        //                            ClassDeleted = ConfigurationManager.AppSettings["ClassActivated"].ToString();
        //                        else
        //                            ClassDeleted = ConfigurationManager.AppSettings["ClassDeactivated"].ToString();
        //                        Body = PopulateBodyClassForCustomer(ClassName, Coach, Location, StartDate, Enddate, CustomerName, Remarks, Language, ClassDeleted, ClassType, RecurringDay, BookingDate);

        //                        isSend = SmtpMail.SendEmail(CustomerEmail, ClassDeleted, Body);
        //                    }

        //                    for (int k = 0; k < dt_coch.Rows.Count; k++)
        //                    {
        //                        string CoachName = (dt_coch.Rows[k]["Name"].ToString());
        //                        string CoachEmail = (dt_coch.Rows[k]["CoachEmail"].ToString());
        //                        string Language = (dt_coch.Rows[k]["LanguageName"].ToString());
        //                        Body = PopulateBodyClassForCoach(ClassName, Location, StartDate, Enddate, CoachName, Remarks, Language, ClassDeleted, ClassType, RecurringDay, BookingDate);
        //                        WriteLog("ClassScheduleNotification....");
        //                        isSend = SmtpMail.SendEmail(CoachEmail, ClassDeleted, Body);
        //                    }
        //                }

        //                ls_SQL = "UPDATE tbl_ClassSchedule " +
        //                         "   SET IsEmailSent     = 1" +
        //                         "      ,EmailSentDate = GetUTCDate()" +
        //                         " WHERE Id  = " + l_dt.Rows[i]["Id"].ToString();
        //                l_DB.Execute(ls_SQL);
        //            }
        //            catch (Exception ex)
        //            {
        //                WriteLog(ex.ToString());
        //            }
        //        }
        //    }

        //    l_DB.CloseDB();
        //}
        public static DataTable GetDistinctRecords(DataTable dt, string[] Columns)
        {
            DataTable dtUniqRecords = new DataTable();
            dtUniqRecords = dt.DefaultView.ToTable(true, Columns);
            return dtUniqRecords;
        }
        private string PopulateBodyClassForCustomer(string ClassName, string Coach, string Location, string StartDate, string Enddate, string CustomerName, string Remarks, string Language, string Status, string ClassType, string RecurringDay, string BookingDate)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {
                //string ls_Path = ConfigurationManager.AppSettings["ClassDeleteCustomer"].ToString();
                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishClassDeleteCustomer"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChineseClassDeleteCustomer"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapaneseClassDeleteCustomer"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaClassDeleteCustomer"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanClassDeleteCustomer"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiClassDeleteCustomer"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnameseClassDeleteCustomer"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerClassDeleteCustomer"].ToString();
                        break;
                }
                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{ClassName}", ClassName);
                body = body.Replace("{ClassType}", ClassType);
                body = body.Replace("{RecurringDays}", RecurringDay);
                body = body.Replace("{BookingDate}", BookingDate);
                body = body.Replace("{Coach}", Coach);
                body = body.Replace("{Location}", Location);
                body = body.Replace("{StartDate}", StartDate);
                body = body.Replace("{EndDate}", Enddate);
                body = body.Replace("{Status}", Status);
                body = body.Replace("{Remark}", Remarks);
                body = body.Replace("{UserName}", CustomerName);
            }
            catch (Exception ex)
            {
                //WriteLog("Populate body");
                //WriteLog(ex.ToString());
            }
            return body;

        }
        private string PopulateBodyClassForCoach(string ClassName, string Location, string StartDate, string Enddate, string CoachName, string Remarks, string Language, string Status, string ClassType, string RecurringDay, string BookingDate)
        {
            string body = string.Empty;
            string ls_Path = string.Empty;
            try
            {
                // ls_Path = ConfigurationManager.AppSettings["ClassDeleteCoach"].ToString();
                switch (Language)
                {
                    case "English":
                        ls_Path = ConfigurationManager.AppSettings["EnglishClassDeleteCoach"].ToString();
                        break;

                    case "Chinese":
                        ls_Path = ConfigurationManager.AppSettings["ChineseClassDeleteCoach"].ToString();
                        break;

                    case "Japanese":
                        ls_Path = ConfigurationManager.AppSettings["JapaneseClassDeleteCoach"].ToString();
                        break;

                    case "Bahasa":
                        ls_Path = ConfigurationManager.AppSettings["BahasaClassDeleteCoach"].ToString();
                        break;

                    case "Korean":
                        ls_Path = ConfigurationManager.AppSettings["KoreanClassDeleteCoach"].ToString();
                        break;

                    case "Thai":
                        ls_Path = ConfigurationManager.AppSettings["ThaiClassDeleteCoach"].ToString();
                        break;
                    case "Vietnamese":
                        ls_Path = ConfigurationManager.AppSettings["VietnameseClassDeleteCoach"].ToString();
                        break;

                    case "Khmer":
                        ls_Path = ConfigurationManager.AppSettings["KhmerClassDeleteCoach"].ToString();
                        break;
                }
                using (StreamReader reader = new StreamReader(ls_Path))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{ClassName}", ClassName);
                body = body.Replace("{ClassType}", ClassType);
                body = body.Replace("{RecurringDays}", RecurringDay);
                body = body.Replace("{BookingDate}", BookingDate);
                body = body.Replace("{Location}", Location);
                body = body.Replace("{StartDate}", StartDate);
                body = body.Replace("{EndDate}", Enddate);
                body = body.Replace("{Status}", Status);
                body = body.Replace("{Remark}", Remarks);
                body = body.Replace("{UserName}", CoachName);
            }
            catch (Exception ex)
            {
                //WriteLog("Populate body");
                //WriteLog(ex.ToString());
            }
            return body;

        }
    }
}
