﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Net;

namespace FitClubService
{
    class SmtpMail
    {
        public static void GetTemplate(string as_Code, out string as_Subject, out string as_Body)
        {
            string ls_Path = ConfigurationManager.AppSettings["EmailTemplate"].ToString();
            string ls_File = as_Code;
            StreamReader sr = new StreamReader(ls_Path + "\\" + ls_File + ".htm");
            as_Body = sr.ReadToEnd();
            sr.Close();

            sr = new StreamReader(ls_Path + "\\" + ls_File + ".txt");
            as_Subject = sr.ReadToEnd();
            sr.Close();
        }

        public static bool SendEmail(string as_ToList, string as_CCList, string as_Subject, string as_Body)
        {
            bool isSend = false;
            try
            {

                string ls_EmailServer = System.Configuration.ConfigurationManager.AppSettings["EmailServer"].ToString();
                string ls_EmailAddress = System.Configuration.ConfigurationManager.AppSettings["EmailAddress"].ToString();
                string ls_EmailName = System.Configuration.ConfigurationManager.AppSettings["EmailName"].ToString();
                string ls_SiteURL = System.Configuration.ConfigurationManager.AppSettings["WebURL"].ToString();
                string ls_SiteURLWin = System.Configuration.ConfigurationManager.AppSettings["AppURL"].ToString();

                MailMessage m = new MailMessage();
                MailAddress ma = new MailAddress(ls_EmailAddress, ls_EmailName);
                m.From = ma;

                string[] ls_ToList = as_ToList.Split(";".ToCharArray());
                foreach (string ls_To in ls_ToList)
                {
                    m.To.Add(ls_To);
                }

                string[] ls_CCList = as_CCList.Split(";".ToCharArray());
                foreach (string ls_CC in ls_CCList)
                {
                    if (ls_CC != "")
                    {
                        m.To.Add(ls_CC);
                    }
                }

                m.Subject = as_Subject;
                m.IsBodyHtml = true;
                m.Body = as_Body;

                SmtpClient smtp = new SmtpClient(ls_EmailServer, 25);
                smtp.Send(m);
                isSend = true;

            }
            catch (Exception ex)
            {
                isSend = false;
                WriteLog(ex.ToString());
            }
            return isSend;
        }

        public static bool SendEmail(string as_ToList, string as_Subject, string as_Body)
        {
            bool isSend = false;
            try
            {

                string ls_EmailServer = System.Configuration.ConfigurationManager.AppSettings["EmailServer"].ToString();
                string ls_EmailAddress = System.Configuration.ConfigurationManager.AppSettings["EmailAddress"].ToString();
                string ls_EmailName = System.Configuration.ConfigurationManager.AppSettings["EmailName"].ToString();

                MailMessage m = new MailMessage();
                MailAddress ma = new MailAddress(ls_EmailAddress, ls_EmailName);
                m.From = ma;

                string[] ls_ToList = as_ToList.Split(";".ToCharArray());
                foreach (string ls_To in ls_ToList)
                {
                    m.To.Add(ls_To);
                }

                m.Subject = as_Subject;
                m.IsBodyHtml = true;
                m.Body = as_Body;

                SmtpClient smtp = new SmtpClient(ls_EmailServer, 25);
                smtp.Send(m);
                isSend = true;

                //SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["EmailServer"].ToString());
                //client.UseDefaultCredentials = false;
                //client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"].ToString());
                //client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                //client.UseDefaultCredentials = false;
                //client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                //MailMessage m = new MailMessage();

                //m.From = new MailAddress(ConfigurationManager.AppSettings["UserId"].ToString());
                //m.To.Add(as_ToList);

                //m.Subject = as_Subject;
                //m.IsBodyHtml = true;
                //m.Body = as_Body;
                //client.Send(m);
                //client.Dispose();
                //m.Dispose();
                //isSend = true;

            }
            catch (Exception ex)
            {
                isSend = false;
                WriteLog(ex.ToString());
            }
            return isSend;
        }

        public static void WriteLog(string as_Text)
        {
            string ls_File = ConfigurationManager.AppSettings["LogFile"].ToString();
            StreamWriter sw = new StreamWriter(ls_File, true);
            sw.WriteLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + as_Text);
            sw.Close();
        }
    }
}
